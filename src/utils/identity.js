import {Button} from "react-bootstrap";
import React from "react";
import {formatDate} from "./date";

/**
 * Format the identity to be shown properly in SummaryList Component
 * @param {identity} identity
 * @returns {{head: string id: integer, body: string, link: string}}
 */
export function formatIdentity(identity) {
    return {
        id: identity.identity_id,
        head: identity.full_name,
        body: `Né(e) le ${identity.birth_date}`,
        link: `/identities/${identity.identity_id}`
    };
}

/**
 * Format the identity to be shown properly in SummaryList Component
 * with a right custom component
 * @param {identity} identity
 * @param {JSX.Element} component
 * @returns {{head: *, id: *, body: string, rightComponent}}
 */
export function formatIdentityWithComponent(identity, component) {
    return {
        id: identity.identity_id,
        head: identity.full_name,
        body: `Né(e) le ${identity.birth_date}`,
        rightComponent: component
    };
}

/**
 * Format the identity to be shown properly in SummaryList Component
 * with a right component that has a callback
 *
 * Callback must take an argument containing target identity
 * @param {identity} identity
 * @param {function} callback
 * @param {string} content
 * @returns {{head: *, id: *, body: string, rightComponent}}
 */
export function formatIdentityWithCallback(identity, callback, content) {
    return {
        id: identity.identity_id,
        head: identity.full_name,
        body: `Né(e) le ${formatDate(identity.birth_date)}`,
        rightComponent: <Button onClick={() => callback(identity)} variant={"outline-info"} size={"sm"}>{content}</Button>
    };
}

