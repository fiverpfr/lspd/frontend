/**
 * Format full date (with hours) to be shown properly
 * dd/mm/YYYY HH:mm
 * @param dateString
 * @returns {string}
 */
export function formatFullDate(dateString) {
    let date = new Date(dateString);
    let day = (date.getDay()).toString().padStart(2, "0");
    // date.getMonth is starting from 0, so needs to add 1 to be human readable
    let month = (date.getMonth()+1).toString().padStart(2, "0");
    return `${day}/${month}/${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}`;
}

/**
 * Format date to be shown properly: dd/mm/YYYY
 * @param dateString
 * @returns {string}
 */
export function formatDate(dateString) {
    return formatFullDate(dateString).split(" ")[0];
}