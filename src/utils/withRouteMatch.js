import React from "react";
import {Switch, useRouteMatch} from "react-router-dom";

/**
 * Higher Order Component to provide path & url
 *
 * @param {Component} ComposedComponent
 *
 * @returns {Component} - The new composed component
 */
const withRouteMatch = ComposedComponent => function withRouteMatch(props) {
    let { path, url } = useRouteMatch();

    return (
        <Switch>
            <ComposedComponent path={path} url={url} {...props} />
        </Switch>
    );
};

export default withRouteMatch;