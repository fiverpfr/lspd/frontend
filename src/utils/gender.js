const genders = [
    "Homme",
    "Femme",
    "Autre"
];

export default function getGenderName(gender_id) {
    if (gender_id > genders.length-1)
        return "Inconnu";

    return genders[gender_id];
}