import React from "react";
import { Context } from "../providers/authProvider";

/**
 * Higher Order Component to provide authProvider actions and data
 *
 * @param {Component} ComposedComponent
 *
 * @returns {Component} - The new composed component
 */
const withAuth = ComposedComponent => (props) => {
    return (
        <Context.Consumer>
            {({data, actions}) => {
                return (
                    <ComposedComponent data={data} actions={actions} {...props} />
                );
            }}
        </Context.Consumer>
    );
};

export default withAuth;