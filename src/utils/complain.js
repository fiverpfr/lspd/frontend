/**
 * Format the complain to be shown properly
 * @param {complain} complain
 */
export function formatComplain(complain) {
    let final = {};
    let first_complainer = complain.identities.find(p => p.complain_identities.type === "complainer");
    let first_target = complain.identities.find(p => p.complain_identities.type === "target");
    let nb_complainer = complain.identities.filter(p => p.complain_identities.type === "complainer").length;
    let nb_target = complain.identities.filter(p => p.complain_identities.type === "target").length;

    if (first_target === undefined || first_complainer === undefined) {
        final.head = "PLAINTE INVALIDE";
    }
    else if (nb_complainer > 1 || nb_target > 1) {
        final.head = `${first_complainer.full_name} `;

        if (nb_complainer > 1)
            final.head += ` et ${nb_complainer-1} autre(s) plaignant(s) `;

        final.head += "contre ";
        final.head += `${first_target.full_name} `;

        if (nb_target > 1)
            final.head += `et ${nb_target-1} autre(s) cible(s)`;
    }
    else {
        final.head = `${first_complainer.full_name} contre `;
        final.head += `${first_target.full_name}`;
    }

    final.body = complain.object;
    final.id = complain.complain_id;
    final.link = `/complains/${complain.complain_id}`;
    final.description = complain.description;

    return final;
}