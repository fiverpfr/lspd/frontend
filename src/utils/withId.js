import React from "react";
import {useParams} from "react-router-dom";

/**
 * Higher Order Component to add in props an id provided
 *
 * @param {Component} ComposedComponent
 *
 * @returns {Component} - The new composed component
 */
const withId = ComposedComponent => (props) => {
    let { id } = useParams();

    if (isNaN(id))
        return null;

    return (
        <ComposedComponent  id={parseInt(id)} {...props} />
    );
};

export default withId;