import React, {Component} from "react";
import {Link, NavLink} from "react-router-dom";
import Container from "./styled";
import {Nav} from "react-bootstrap";
import withRouteMatch from "../../utils/withRouteMatch";
import {routes} from "../../_config/routes";
import PropTypes from "prop-types";

class NavigationTab extends Component {

    render() {
        const mainRoute = routes.find(c => c.path === this.props.url);
        const subroutes = mainRoute?.subroutes;
        if (subroutes === undefined)
            return null;

        return (
            <Container className="bg-white shadow">
                <Nav className="nav nav-underline" aria-label="Secondary navigation">
                    <NavLink
                        key={mainRoute.path}
                        alt={mainRoute.name}
                        as={Link}
                        to={`${mainRoute.path}`}
                        className={"nav-link"}
                    >
                        {mainRoute.name}
                    </NavLink>

                    {
                        subroutes.map(c =>
                            c.name !== null
                                ? <NavLink
                                    key={c.path}
                                    as={Link}
                                    alt={c.name}
                                    to={`${this.props.url}/${c.path}`}
                                    className={"nav-link"}
                                >
                                    {c.name}
                                </NavLink>
                                : null
                        )
                    }
                </Nav>
            </Container>
        );
    }
}

NavigationTab.propTypes = {
    url: PropTypes.string.isRequired
};

export default withRouteMatch(NavigationTab);