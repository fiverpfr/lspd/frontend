import React, {Component} from "react";
import withAuth from "../utils/withAuth";
import {Form} from "react-bootstrap";
import PropTypes from "prop-types";

class ToggleAction extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: props.current
        };
    }

    handleChange() {
        this.setState({checked: !this.state.checked});

        if (!this.props.onChange())
            this.setState({checked: !this.state.checked});
    }

    render() {
        return (
            <Form>
                <Form.Check
                    type={"switch"}
                    checked={this.state.checked}
                    id={this.props.id}
                    label={this.state.checked ? "activé" : "désactivé"}
                    onChange={this.handleChange.bind(this)}
                />
            </Form>
        );
    }
}

ToggleAction.propTypes = {
    id: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired
};

export default withAuth(ToggleAction);