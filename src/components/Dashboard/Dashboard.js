import React, {Component} from "react";
import withAuth from "../../utils/withAuth";
import BodyHeadNotification from "../BodyHeadNotification";

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.data = props.data;
    }

    render() {
        return (
            <>
                <BodyHeadNotification
                    text={`Bon retour parmis nous, ${this.data.primary_rank?.name ?? "inconnu"} ${this.data.last_name}`}
                    subText={"Membre de la 7ème compagnie depuis 1995"}
                />
            </>
        );
    }
}

export default withAuth(Dashboard);