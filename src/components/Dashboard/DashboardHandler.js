import React, {Component} from "react";
import {Route, Switch} from "react-router-dom";
import withRouteMatch from "../../utils/withRouteMatch";
import {Redirect} from "react-router-dom";
import Dashboard from "./Dashboard";
import {routes} from "../../_config/routes";

class DashboardHandler extends Component {
    constructor(props) {
        super(props);
        this.subroutes = routes.find(c => c.path === this.props.url)?.subroutes;
    }

    render() {
        return (
            <Switch>
                <Route
                    path={"/dashboard"}
                    exact
                >
                    <Dashboard />
                </Route>

                {
                    this.subroutes
                        ? this.subroutes.map(c =>
                            (
                                <Route
                                    key={`${this.props.url}/${c.path}`}
                                    path={`${this.props.url}/${c.path}`}
                                    exact
                                >
                                    {
                                        c.component
                                            ? <c.component />
                                            : <Redirect to={c.redirect} />
                                    }
                                </Route>
                            )
                        )
                        : null
                }
            </Switch>
        );
    }
}

export default withRouteMatch(DashboardHandler);