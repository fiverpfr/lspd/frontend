import React, {Component} from "react";
import {Nav, Navbar, NavLink} from "react-bootstrap";
import lspdLogo from "../images/lspd.png";
import NavbarToggle from "react-bootstrap/NavbarToggle";
import NavbarCollapse from "react-bootstrap/NavbarCollapse";
import {Link} from "react-router-dom";
import withAuth from "../utils/withAuth";
import withRouteMatch from "../utils/withRouteMatch";
import NavigationTab from "./NavigationTab";
import * as PropTypes from "prop-types";

class NavigationBar extends Component {
    constructor(props) {
        super(props);
        let { actions, data } = props;

        this.actions = actions;
        this.data = data;
        this.showUserData = this.showUserData.bind(this);
    }

    showUserData() {
        return (
            <Nav>
                <Nav.Link href="#username">{this.data.full_name}</Nav.Link>
                <Nav.Link onClick={this.actions.onLogout}>Se déconnecter</Nav.Link>
            </Nav>
        );
    }

    render() {
        if (!this.actions.isLoggedIn())
            return null;

        return (
            <>
                <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                    <Navbar.Brand href="#home">
                        <img
                            src={lspdLogo}
                            width="48"
                            className="d-inline-block align-top"
                            alt="Logo LSPD"
                        />
                    </Navbar.Brand>
                    <NavbarToggle aria-controls="responsive-navbar-nav" />
                    <NavbarCollapse id="responsive-navbar-nav">
                        <Nav className="mr-auto">
                            <NavLink as={Link} to="/dashboard" className={this.props.url === "/dashboard" ? "active" : null}>Accueil</NavLink>
                            <NavLink as={Link} to="/complains" className={this.props.url === "/complains" ? "active" : null}>Préjudices</NavLink>
                            {
                                this.data.perms["user.list"] &&
                                <NavLink as={Link} to="/users" className={this.props.url === "/users" ? "active" : null}>Administration</NavLink>
                            }
                        </Nav>
                        {this.showUserData()}
                    </NavbarCollapse>
                </Navbar>
                <NavigationTab />
            </>
        );
    }
}

NavigationBar.propTypes = {
    url: PropTypes.string.isRequired,
    actions: PropTypes.objectOf(PropTypes.func),
    data: PropTypes.objectOf(PropTypes.any)
};

export default withAuth(withRouteMatch(NavigationBar));