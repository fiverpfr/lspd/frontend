import React, {Component} from "react";
import {MediumFiveSpinner} from "./FiveSpinner";
import * as Proptypes from "prop-types";
import {Link} from "react-router-dom";

// eslint-disable-next-line react/prop-types
function Item({head, body, link, rightComponent}) {
    return (
        <div className="d-flex text-muted pt-3">
            <svg className="bd-placeholder-img flex-shrink-0 me-2 rounded" width="32" height="32"
                xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: 32x32"
                preserveAspectRatio="xMidYMid slice" focusable="false"><title>{head}</title>
                <rect width="100%" height="100%" fill="#000000"/>
                <text x="25%" y="50%" fill="#000000" dy=".3em">{head}</text>
            </svg>
            <div className="pb-3 mb-0 small lh-sm border-bottom w-100">

                <div className="d-flex justify-content-between">
                    <strong className="text-gray-dark">{head}</strong>
                    {link ? <Link key={link} to={link}>Voir</Link> : null}
                    {rightComponent ? rightComponent : null}
                </div>
                {body}
            </div>
        </div>
    );
}

function CenterSpinner() {
    return (
        <div align={"center"}>
            <MediumFiveSpinner />
        </div>
    );
}

class SummaryList extends Component {
    constructor(props) {
        super(props);
        let { items } = this.props;

        if (items === undefined)
            this.state = {inLoading: true, canShow: false};
        else
            this.state = {canShow: true};
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.inLoading === true)
            setTimeout(() => this.setState({canShow: true}), 500);
    }

    render() {
        return (
            <div className="my-3 p-3 bg-white rounded shadow-sm">
                <h6 className="border-bottom pb-2 mb-0">{this.props.title}</h6>
                {
                    this.props.items && this.state.canShow
                        ? this.props.items.map(item =>
                            <Item key={item.id} {...item} />
                        )
                        : <CenterSpinner />
                }
            </div>
        );
    }
}

SummaryList.propTypes = {
    title: Proptypes.string.isRequired,
    items: Proptypes.arrayOf(Proptypes.shape({
        head: Proptypes.string.isRequired,
        body: Proptypes.string.isRequired,
        id: Proptypes.number.isRequired,
        link: Proptypes.string
    }))
};

export default SummaryList;