import React, {Component} from "react";
import {Redirect, Route} from "react-router-dom";
import withAuth from "../utils/withAuth";
import Loading from "./Loading";

class CustomRoute extends Component {
    render() {
        const {
            actions: {isLoggedIn, isPendingLogin},
            redirectTo = "/login",
            component: Component,
            isProtected,
            ...rest
        } = this.props;
        return (
            <Route
                {...rest}
            >
                {props => {
                    if (isLoggedIn() || !isProtected)
                        return <Component {...props} />;
                    else if (isPendingLogin())
                        return <Loading isActive={true}/>;
                    else
                        return <Redirect to={redirectTo}/>;
                }
                }
            </Route>
        );
    }
}

export default withAuth(CustomRoute);