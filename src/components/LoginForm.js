import React from "react";
import {useFormik} from "formik";
import * as PropTypes from "prop-types";
import {Button, Form, InputGroup} from "react-bootstrap";
import fiveLoading from "../images/five_loading.gif";

function LoginForm({onSubmit, initialValues, validate, loading}) {
    const formik = useFormik({
        initialValues: initialValues,
        onSubmit: onSubmit,
        validate: validate
    });

    return (
        <Form noValidate onSubmit={formik.handleSubmit}>
            <Form.Group>
                <Form.Label>Username</Form.Label>
                <InputGroup>
                    <Form.Control
                        type="text"
                        placeholder="Nom d'utilisateur, eg: John_Doe"
                        aria-describedby="username"
                        name="username"
                        value={formik.values.username}
                        onChange={formik.handleChange}
                        isInvalid={!!formik.errors.username}
                    />
                    <Form.Control.Feedback type="invalid" tooltip>
                        {formik.errors.username}
                    </Form.Control.Feedback>
                </InputGroup>
            </Form.Group>

            <Form.Group controlId="validationFormikUsername2">
                <Form.Label>Mot de passe</Form.Label>
                <InputGroup>
                    <Form.Control
                        type="password"
                        placeholder="Mot de passe"
                        aria-describedby="password"
                        name="password"
                        value={formik.values.password}
                        onChange={formik.handleChange}
                        isInvalid={!!formik.errors.password}
                    />
                    <Form.Control.Feedback type="invalid" tooltip>
                        {formik.errors.password}
                    </Form.Control.Feedback>
                </InputGroup>
            </Form.Group>

            {
                loading
                    ?
                    (
                        <Button variant="primary" disabled block>
                            <img width="32px" src={fiveLoading} alt="FiveRP Loading" />
							En cours...
                        </Button>
                    )
                    :
                    <Button type={"submit"} variant={"primary"} block>Envoyer</Button>
            }
        </Form>
    );
}

LoginForm.propTypes = {
    onSubmit: PropTypes.any,
    initialValues: PropTypes.any,
    validate: PropTypes.func,
    loading: PropTypes.bool
};

export default LoginForm;