import React, {Component} from "react";
import {FormControl, FormLabel, FormText, Container} from "react-bootstrap";
import SummaryList from "../SummaryList";
import backend from "../../_config/backend";
import {formatIdentity} from "../../utils/identity";
import {toast} from "react-toastify";

export function IdentitiesSearchForm({onChange, value}) {
    return (
        <Container fluid>
            <FormLabel htmlFor="search">Rechercher quelqu'un</FormLabel>
            <FormControl
                type="text"
                id="search"
                aria-describedby="searchHelpBlock"
                placeholder={"Ma super recherche..."}
                value={value}
                onChange={onChange}
            />
            <FormText id="searchHelpBlock" muted>
              La recherche se fera sur toutes les identités <b>disponible</b>.
            </FormText>
        </Container>
    );
}

class IdentitiesSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchValue: "",
            identities: undefined
        };
    }

    onChange(e) {
        let search = e.target.value;
        this.setState({searchValue: search});

        if (search.length >= 3)
            this.searchIdentities(search);
    }

    searchIdentities(query) {
        backend.identities_search(query)
            .then(result => {
                result.rows = result.rows.map(c => formatIdentity(c));

                this.setState({ identities: result.rows });
            })
            .catch(() => {
                toast.dark("❌ Une erreur s'est produire, veuillez ré-essayer plus tard");
            });
    }

    render() {
        return (
            <>
                <IdentitiesSearchForm onChange={this.onChange.bind(this)} value={this.state.searchValue} />
                {
                    this.state.identities
                        ? <SummaryList title={"Identités trouvées"} items={this.state.identities} />
                        : null
                }
            </>
        );
    }
}

export default IdentitiesSearch;