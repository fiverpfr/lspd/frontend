import React, {Component} from "react";
import {toast} from "react-toastify";
import IdentityForm from "./IdentityForm";
import Loading from "../Loading";
import backend from "../../_config/backend";
import withId from "../../utils/withId";
import {Redirect} from "react-router-dom";



class IdentityEdit extends Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount() {
        let { id } = this.props;

        backend.identity(id)
            .then(identity => {
                this.setState({
                    firstName: identity.first_name,
                    lastName: identity.last_name,
                    birthDate: identity.birth_date,
                    gender: identity.gender,
                    phoneNumber: identity.phone_number
                });
            })
            .catch(() => {
                toast.dark("❌ Cette identité n'existe pas, ou est incorrecte");
                this.setState({redirectTo: "/dashboard"});
            });
    }

    onSubmit(args, functions) {
        const {
            firstName,
            lastName,
            birthDate,
            gender,
            phoneNumber
        } = args;

        const {
            setSubmitting
        } = functions;

        backend.identity_update(this.props.id, {
            first_name: firstName,
            last_name: lastName,
            birth_date: birthDate,
            gender: gender,
            phone_number: phoneNumber
        })
            .then(() => {
                toast.dark("🚀 Le profil a été mis à jour");
                this.setState({redirectTo: `/identities/${this.props.id}`});
                setSubmitting(false);
            })
            .catch(err => {
                this.handleSubmitError(err.errors[0]?.path);
                this.setState({redirectTo: `/identities/${this.props.id}`});
                setSubmitting(false);
            });
    }

    handleSubmitError(errorPath) {
        switch(errorPath) {
            case "nameUnique":
                toast.dark("❌ Cette identité existe déjà");
                break;
            case "phoneNumberFormat":
                toast.dark("❌ Le format du numéro est mauvais");
                break;
            case "birthdateValid":
                toast.dark("❌ La date de naissance est invalide");
                break;
            case "genderValid":
                toast.dark("❌ Votre genre est invalide (H/F/A)");
                break;
            default:
                toast.dark("❌ Impossible de mettre à jour le profil tout de suite");
                break;
        }
    }

    render() {
        return (
            <>
                <Loading isActive={this.state === null} />
                {
                    this.state?.redirectTo
                        ? <Redirect to={this.state.redirectTo} />
                        : <IdentityForm onSubmit={this.onSubmit} initialValues={this.state} />
                }
            </>
        );
    }
}

export default withId(IdentityEdit);