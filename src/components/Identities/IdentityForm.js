import * as yup from "yup";
import {useFormik} from "formik";
import {Button, Col, Form} from "react-bootstrap";
import {SmallFiveSpinner} from "../FiveSpinner";
import React from "react";

const schema = yup.object({
    firstName: yup.string()
        .required("Prénom nécessaire"),
    lastName: yup.string()
        .required("Nom nécessaire"),
    gender: yup.number()
        .required("Genre nécessaire"),
    birthDate: yup.date()
        .max(2002, "L'année de naissance maximum est 2002")
        .min(1940, "L'année de naissance minimum est 1940")
        .required("Date nécessaire"),
    phoneNumber: yup.string()
        .matches("[0-9]{4}-[0-9]{4}", "Le format d'un numéro est XXXX-XXXX")
});

const IdentityForm = ({initialValues, onSubmit}) => {
    const formik = useFormik({
        initialValues: initialValues || {
            firstName: "test",
            lastName: "test",
            gender: 0,
            phoneNumber: "1234-1234",
            birthDate: "2000-01-01"
        },
        onSubmit: onSubmit,
        validationSchema: schema
    });
    const { values, handleChange, errors, handleSubmit, isSubmitting } = formik;

    return (
        <Form noValidate onSubmit={handleSubmit}>
            <Form.Row>
                <Form.Group as={Col} sm={12} md={6} lg={6} controlId={"validationFirstName"}>
                    <Form.Label>Prénom</Form.Label>
                    <Form.Control
                        type="text"
                        name={"firstName"}
                        value={values.firstName}
                        onChange={handleChange}
                        isInvalid={errors.firstName}
                        placeholder="Prénom"
                    />
                    <Form.Control.Feedback type={"invalid"}>
                        {errors.firstName}
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group as={Col} sm={12} md={6} lg={6}  controlId={"validationLastName"}>
                    <Form.Label>Nom</Form.Label>
                    <Form.Control
                        type="text"
                        name={"lastName"}
                        value={values.lastName}
                        onChange={handleChange}
                        isInvalid={errors.lastName}
                        placeholder="Nom"
                    />
                    <Form.Control.Feedback type={"invalid"}>
                        {errors.lastName}
                    </Form.Control.Feedback>
                </Form.Group>
            </Form.Row>
            <Form.Row>
                <Form.Group as={Col} sm={12} md={6} lg={4}  controlId={"validationBirthDate"}>
                    <Form.Label>Date de naissance</Form.Label>
                    <Form.Control
                        type="date"
                        placeholder="dd/mm/YYYY"
                        name={"birthDate"}
                        value={values.birthDate}
                        onChange={handleChange}
                        isInvalid={errors.birthDate}
                    />
                    <Form.Control.Feedback type={"invalid"}>
                        {errors.birthDate}
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group as={Col} sm={12} md={6} lg={4}  controlId={"validationGender"}>
                    <Form.Label>Genre</Form.Label>
                    <Form.Control
                        as={"select"}
                        name={"gender"}
                        value={values.gender}
                        onChange={handleChange}
                        isInvalid={errors.gender}
                    >
                        <option disabled value>Sélectionnez un genre</option>
                        <option value={0}>Homme</option>
                        <option value={1}>Femme</option>
                        <option value={2}>Autre</option>
                    </Form.Control>
                    <Form.Control.Feedback type={"invalid"}>
                        {errors.gender}
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group as={Col} sm={12} md={6} lg={4}  controlId={"validationPhoneNumber"}>
                    <Form.Label>Numéro de téléphone</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="1234-1234"
                        name={"phoneNumber"}
                        onChange={handleChange}
                        value={values.phoneNumber}
                        isInvalid={errors.phoneNumber}
                    />
                    <Form.Control.Feedback type={"invalid"}>
                        {errors.phoneNumber}
                    </Form.Control.Feedback>
                </Form.Group>
            </Form.Row>
            <Button disabled={isSubmitting} variant={"primary"} type={"submit"}>
                {isSubmitting ? <SmallFiveSpinner /> : "Valider"}
            </Button>
        </Form>
    );
};

export default IdentityForm;