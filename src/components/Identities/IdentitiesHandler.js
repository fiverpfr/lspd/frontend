import React, {Component} from "react";
import {Route, Switch} from "react-router-dom";
import withRouteMatch from "../../utils/withRouteMatch";
import {routes} from "../../_config/routes";
import PropTypes from "prop-types";
import RedirectToDashboard from "../RedirectToDashboard";

class IdentitiesHandler extends Component {

    /**
     * Handling route like that simplify life
     * to know where subRoutes are and to
     * make for some handlers some custom
     * subRoutes.
     *
     *  Recursive path are handled as you don't have to
     * declare every paths in every handler. You only
     * have to declare your paths in /__config/routes.js
     *
     * @returns {JSX.Element|null}
     */
    constructor(props) {
        super(props);
        this.subroutes = routes.find(c => c.path === this.props.url)?.subroutes;
    }

    render() {
        return (
            <Switch>
                <Route
                    path={"/identities"}
                    exact
                >
                    <RedirectToDashboard />
                </Route>

                {
                    this.subroutes
                        ? this.subroutes.map(c =>
                            (
                                <Route
                                    key={`${this.props.url}/${c.path}`}
                                    path={`${this.props.url}/${c.path}`}
                                    exact={c.exact ?? false}
                                >
                                    <c.component />
                                </Route>
                            )
                        )
                        : null
                }
            </Switch>
        );
    }
}

IdentitiesHandler.propTypes = {
    url: PropTypes.string
};

export default withRouteMatch(IdentitiesHandler);