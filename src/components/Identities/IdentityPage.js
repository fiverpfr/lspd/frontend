import React from "react";
import {Badge, Card, Col, ListGroup, ListGroupItem, Row} from "react-bootstrap";
import backend from "../../_config/backend";
import {MediumFiveSpinner} from "../FiveSpinner";
import getGenderName from "../../utils/gender";
import PropTypes from "prop-types";
import SummaryList from "../SummaryList";
import {formatComplain} from "../../utils/complain";
import withId from "../../utils/withId";
import {Link, Redirect} from "react-router-dom";
import {formatFullDate} from "../../utils/date";
import {toast} from "react-toastify";
import withAuth from "../../utils/withAuth";

class IdentityPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.canDelete = this.props.data.perms["identity.delete"];
    }


    componentDidMount() {
        let id = this.props.id;

        if (isNaN(id))
        {
            toast.dark("❌ Plainte incorrect");
            this.setState({redirectTo: "/dashboard"});
            return;
        }

        Promise.all([
            backend.identity(id),
            backend.identity_complains(id)
        ])
            .then(([ identity, complains ]) => {
                let complains_list = complains.rows.map(c => formatComplain(c));
                this.setState({...identity, complains: complains_list, complains_count: complains.count});
            })
            .catch(() => {
                toast.dark("❌ Une erreur s'est produite ré-essayez plus tard");
                this.setState({redirectTo: "/dashboard"});
            });
    }

    deleteIdentity() {
        backend.identity_delete(this.props.id)
            .then(() => {
                toast.dark("🤘 Identité supprimée avec succès");
                this.setState({redirectTo: "/identities"});
            })
            .catch(() => toast.dark("❌ Une erreur s'est produite, impossible de supprimer"));
    }

    render_loading() {
        return (
            <Row>
                <Col lg>
                    <Card style={{width: "100%"}}>
                        <Card.Header className={"text-justify"}>
                            <MediumFiveSpinner />
                        </Card.Header>
                    </Card>
                </Col>
            </Row>
        );
    }

    render() {
        if (this.state.redirectTo)
            return <Redirect to={this.state.redirectTo} />;

        if (!this.state?.full_name)
            return this.render_loading();

        return (
            <>
                <Row>
                    <Col md={"auto"} lg={6} sm={"auto"}>
                        <Card>
                            <Card.Body>
                                <Card.Title>
                                    {this.state.full_name}
                                </Card.Title>
                                <Card.Subtitle className={"mb-2 text-muted"}>Genre: {getGenderName(this.state.gender)}</Card.Subtitle>
                                <Card.Text>

                                </Card.Text>
                                <Card.Footer className={"text-muted"}>Ajouté(e) le {new Date(this.state.createdAt).toLocaleDateString()}</Card.Footer>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={"auto"} lg={6} sm={"auto"}>
                        <Card>
                            <Card.Body>
                                <ListGroup variant={"flush"}>
                                    <ListGroupItem>
                                        <Row>
                                            <Col>
                                                Numéro de téléphone
                                            </Col>
                                            <Col>
                                                {this.state.phone_number}
                                            </Col>
                                        </Row>
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <Row>
                                            <Col>
                                                Date de naissance
                                            </Col>
                                            <Col>
                                                {this.state.birth_date}
                                            </Col>
                                        </Row>
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <Row>
                                            <Col>
                                                Dernière mise à jour
                                            </Col>
                                            <Col>
                                                {formatFullDate(this.state.updatedAt)}
                                            </Col>
                                        </Row>
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <Row>
                                            <Col>
                                                Plainte(s) liée(s)
                                            </Col>
                                            <Col>
                                                {this.state.complains_count}
                                            </Col>
                                        </Row>
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <Row>
                                            <Col>
                                                Actions
                                            </Col>
                                            <Col>
                                                <Link to={`${this.props.id}/edit`}>
                                                    <Badge className="text-right pull-right" variant="secondary">
                                                        Modifier
                                                    </Badge>
                                                </Link>
                                                {
                                                    this.canDelete &&
                                                    <Badge onClick={this.deleteIdentity.bind(this)} className="text-right pull-right" variant="danger">
                                                        Supprimer
                                                    </Badge>
                                                }
                                            </Col>
                                        </Row>
                                    </ListGroupItem>
                                </ListGroup>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col className={"mt-2"} md={"auto"} lg={12} sm={"auto"}>
                        <Card>
                            <Card.Body>
                                {
                                    <SummaryList
                                        title={"Plainte(s) lié(s)"}
                                        items={this.state.complains}
                                    />
                                }
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </>
        );
    }
}

IdentityPage.propTypes = {
    id: PropTypes.number.isRequired
};

export default withAuth(withId(IdentityPage));