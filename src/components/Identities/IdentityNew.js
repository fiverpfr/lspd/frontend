import React, {Component} from "react";
import backend from "../../_config/backend";
import {toast} from "react-toastify";
import IdentityForm from "./IdentityForm";
import {Redirect} from "react-router-dom";

class IdentityNew extends Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(args, functions) {
        const {
            firstName,
            lastName,
            birthDate,
            gender,
            phoneNumber
        } = args;

        const {
            setSubmitting
        } = functions;

        backend.identity_new({
            first_name: firstName,
            last_name: lastName,
            birth_date: birthDate,
            gender: gender,
            phone_number: phoneNumber
        })
            .then(res => {
                setTimeout(() => {
                    toast.dark("🚀 La nouvelle identité est créée");
                    setSubmitting(false);
                    this.setState({redirectTo: `/identities/${res.identity_id}`});
                }, 500);
            })
            .catch(err => {
                setTimeout(() => {
                    this.handleSubmitError(err.errors[0]?.path);
                    setSubmitting(false);
                }, 500);
            });
    }

    handleSubmitError(errorPath) {
        switch(errorPath) {
            case "nameUnique":
                toast.dark("❌ Cette identité existe déjà");
                break;
            case "phoneNumberFormat":
                toast.dark("❌ Le format du numéro est mauvais");
                break;
            case "birthdateValid":
                toast.dark("❌ La date de naissance est invalide");
                break;
            case "genderValid":
                toast.dark("❌ Votre genre est invalide (H/F/A)");
                break;
            default:
                toast.dark("❌ Impossible de mettre à jour le profil tout de suite");
                break;
        }
    }

    render() {
        return (
            <div>
                {
                    this.state?.redirectTo
                        ? <Redirect to={this.state.redirectTo} />
                        : <IdentityForm onSubmit={this.onSubmit} />
                }
            </div>
        );
    }
}

export default IdentityNew;