import * as yup from "yup";
import {useFormik} from "formik";
import {Button, Col, Form} from "react-bootstrap";
import {SmallFiveSpinner} from "../FiveSpinner";
import React from "react";

const schema = yup.object({
    object: yup.string()
        .required("Un objet est nécessaire"),
    description: yup.string()
        .required("Une description est nécessaire")
});

const ComplainForm = ({initialValues, onSubmit, submittable, children, submitButton}) => {
    const formik = useFormik({
        initialValues: initialValues || {
            object: "",
            description: ""
        },
        onSubmit: onSubmit,
        validationSchema: schema
    });
    const { values, handleChange, errors, handleSubmit, isSubmitting } = formik;

    return (
        <Form noValidate onSubmit={handleSubmit}>
            <Form.Row>
                <Form.Group as={Col} controlId={"validationObject"}>
                    <Form.Label>Objet</Form.Label>
                    <Form.Control
                        type="text"
                        name={"object"}
                        value={values.object}
                        onChange={handleChange}
                        isInvalid={errors.object}
                        placeholder="Objet de la plainte"
                    />
                    <Form.Control.Feedback type={"invalid"}>
                        {errors.object}
                    </Form.Control.Feedback>
                </Form.Group>
            </Form.Row>
            <Form.Row>
                <Form.Group as={Col} controlId={"validationDescription"}>
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                        as="textarea"
                        rows={4}
                        placeholder="Description de la plainte"
                        name={"description"}
                        value={values.description}
                        onChange={handleChange}
                        isInvalid={errors.description}
                    />
                    <Form.Control.Feedback type={"invalid"}>
                        {errors.description}
                    </Form.Control.Feedback>
                </Form.Group>
            </Form.Row>
            {children}
            <Button disabled={isSubmitting || submittable} variant={"primary"} className={"mt-2"} type={"submit"}>
                {isSubmitting ? <SmallFiveSpinner /> : (submitButton || "Valider")}
            </Button>
        </Form>
    );
};

export default ComplainForm;