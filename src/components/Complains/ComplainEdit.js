import React, {Component} from "react";
import {toast} from "react-toastify";
import backend from "../../_config/backend";
import withId from "../../utils/withId";
import {Redirect} from "react-router-dom";
import ComplainForm from "./ComplainForm";
import Loading from "../Loading";



class ComplainEdit extends Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount() {
        let { id } = this.props;
        backend.complain(id)
            .then(complain => {
                this.setState({
                    complain: complain
                });
            }).catch(() => {
                toast.dark("❌ Cette plainte n'existe pas, ou est incorrecte");
                this.setState({redirectTo: "/complains"});
            });

    }

    onSubmit(args, functions) {
        const {
            object,
            description
        } = args;

        const {
            setSubmitting
        } = functions;

        backend.complain_update(this.props.id, {
            object: object,
            description: description
        })
            .then(() => {
                toast.dark("🚀 La plainte a été mise à jour");
                this.setState({redirectTo: `/complains/${this.props.id}`});
                setSubmitting(false);
            })
            .catch(() => {
                this.handleSubmitError("❌ Une erreur s'est produite");
                this.setState({redirectTo: `/complains/${this.props.id}`});
                setSubmitting(false);
            });
    }

    render() {
        if (this.state?.redirectTo)
            return <Redirect to={this.state.redirectTo} />;


        return (
            <>
                <Loading isActive={this.state?.complain === null} />
                {
                    this.state?.complain
                    && <ComplainForm onSubmit={this.onSubmit} initialValues={this.state.complain} />
                }
            </>
        );
    }
}

export default withId(ComplainEdit);