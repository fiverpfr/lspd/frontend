import React, {Component} from "react";
import {FormControl, FormLabel, FormText, Container} from "react-bootstrap";
import SummaryList from "../SummaryList";
import backend from "../../_config/backend";
import {formatComplain} from "../../utils/complain";
import {toast} from "react-toastify";

function SearchForm({onChange, value}) {

    return (
        <Container fluid>
            <FormLabel htmlFor="search">Rechercher</FormLabel>
            <FormControl
                type="text"
                id="search"
                aria-describedby="searchHelpBlock"
                placeholder={"Ma super recherche..."}
                value={value}
                onChange={onChange}
            />
            <FormText id="searchHelpBlock" muted>
              La recherche se fera sur toutes les plaintes <b>ouvertes</b>.
            </FormText>
        </Container>
    );
}

class ComplainsSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchValue: "",
            complains: undefined
        };
    }

    onChange(e) {
        let search = e.target.value;
        this.setState({searchValue: search});

        if (search.length >= 3)
            this.searchComplains(search);
    }

    searchComplains(query) {
        backend.complains_search(query)
            .then(result => {
                if (result.count > 0)
                {
                    result = result.rows.map(c => formatComplain(c));
                    this.setState({complains: result});
                }
            })
            .catch(() => toast.dark("❌ Une erreur s'est produite, ré-essayez plus tard"));
    }

    render() {
        return (
            <>
                <SearchForm onChange={this.onChange.bind(this)} value={this.state.searchValue} />
                {
                    this.state.complains
                        ? <SummaryList title={"Plaintes trouvées"} items={this.state.complains} />
                        : null
                }
            </>
        );
    }
}

export default ComplainsSearch;