import React, {Component} from "react";
import {Redirect} from "react-router-dom";
import ComplainForm from "./ComplainForm";
import NewComplainerModal from "./NewComplainerModal";
import {toast} from "react-toastify";
import {formatIdentityWithCallback} from "../../utils/identity";
import SummaryList from "../SummaryList";
import {Col, Row} from "react-bootstrap";
import backend from "../../_config/backend";



class ComplainNew extends Component {
    constructor(props) {
        super(props);

        this.onSubmit = this.onSubmit.bind(this);
        this.onAddIdentity = this.onAddIdentity.bind(this);
        this.state = {
            complainers: [],
            formattedComplainers: [],
            targets: [],
            formattedTargets: []
        };
    }

    onSubmit(args, functions) {
        const {
            object,
            description
        } = args;

        const {
            setSubmitting
        } = functions;

        const {
            complainers,
            targets
        } = this.state;

        let identities = complainers.map(c => ({identity_id: c.identity_id, type: "complainer"}));
        identities = identities.concat(targets.map(c => ({identity_id: c.identity_id, type: "target"})));

        if (object.length === 0 || description.length === 0)
        {
            setTimeout(() => {
                setSubmitting(false);
                toast.dark("❌ Vous devez remplir tous les champs");
            }, 500);
            return;
        }
        else if (complainers.length === 0 || targets.length === 0)
        {
            setTimeout(() => {
                setSubmitting(false);
                toast.dark("❌ Vous devez ajouter au moins un plaignant & un accusé");
            }, 500);
            return;
        }

        backend.complain_new({
            object,
            description,
            complainers: identities
        })
            .then(res => {
                this.setState({redirectTo: `/complains/${res.complain_id}`});
                toast.dark("🚀 Nouvelle plainte créée");
            })
            .catch(() => {
                toast.dark("❌ Une erreur s'est produite, veuillez ré-essayer");
                this.setState({redirectTo: "/complains"});
            });
    }

    /**
     * Validate the new identity & process
     * Must returns a boolean
     * @param {identity} identity
     * @param {"complainer"|"target"} type
     * @returns {boolean} validated
     */
    onAddIdentity(identity, type) {
        let { complainers, targets } = this.state;
        let index_complainer = !!complainers.find(c => c.identity_id === identity.identity_id);
        let index_target = !!targets.find(c => c.identity_id === identity.identity_id);

        if (type === "target" && index_complainer ||
            type === "complainer" && index_target)
        {
            toast.dark("❌ Une identité ne peut être plaignant & un accusé");
            return false;
        }
        else if (index_complainer || index_target)
        {
            toast.dark("❌ Cette identité est déjà dans la liste");
            return false;
        }

        if (type === "complainer")
        {
            this.setState({
                complainers: [...complainers, identity],
                formattedComplainers: [...this.state.formattedComplainers, formatIdentityWithCallback(
                    identity,
                    this.onRemoveIdentity.bind(this),
                    "❌"
                )]
            });
        }
        else
        {
            this.setState({
                targets: [...targets, identity],
                formattedTargets: [...this.state.formattedTargets, formatIdentityWithCallback(
                    identity,
                    this.onRemoveIdentity.bind(this),
                    "❌"
                )]
            });
        }
        toast.dark(`🚀 Identité ajoutée en tant que ${type === "complainer" ? "plaignant" : "accusé"}`);
        return true;
    }

    /**
     * When clicking the cross to remove an identity
     * @param {identity} identity
     */
    onRemoveIdentity(identity) {
        let { complainers, targets, formattedComplainers, formattedTargets } = this.state;
        let complainer = complainers.find(c => c.identity_id === identity.identity_id);
        let target = targets.find(c => c.identity_id === identity.identity_id);

        if (complainer) {
            this.setState({
                complainers: complainers.filter(c => c.identity_id !== identity.identity_id),
                formattedComplainers: formattedComplainers.filter(c => c.id !== identity.identity_id),
            });
        }
        else if (target)
        {
            this.setState({
                targets: targets.filter(c => c.identity_id !== identity.identity_id),
                formattedTargets: formattedTargets.filter(c => c.id !== identity.identity_id),
            });
        }
    }

    render() {
        let canSubmit = this.state.complainers.length > 0 && this.state.targets.length > 0;
        let submitButton = "";

        if (this.state.complainers.length === 0)
            submitButton = "Vous devez ajouter un plaignant";
        else if (this.state.targets.length === 0)
            submitButton = "Vous devez ajouter au moins un accusé";

        if (this.state?.redirectTo)
            return <Redirect to={this.state.redirectTo} />;

        return (
            <ComplainForm onSubmit={this.onSubmit} submittable={!canSubmit} submitButton={submitButton}>
                <NewComplainerModal onAddIdentity={this.onAddIdentity} />
                <Row>
                    <Col sm={12} md={12} lg={12}>
                        <SummaryList title={"Liste des plaignants"} items={this.state.formattedComplainers} />
                    </Col>
                </Row>
                <Row>
                    <Col sm={12} md={12} lg={12}>
                        <SummaryList title={"Liste des accusés"} items={this.state.formattedTargets} />
                    </Col>
                </Row>
            </ComplainForm>
        );
    }
}

export default ComplainNew;