import React, {Component} from "react";
import SummaryList from "../SummaryList";
import backend from "../../_config/backend";
import {formatComplain} from "../../utils/complain";
import {toast} from "react-toastify";

class ComplainsPage extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        backend.me_complains()
            .then(result => {
                this.setState({complains: result.map(c => formatComplain(c))});
            })
            .catch(() => {
                toast.dark("❌ Impossible de charger vos plaintes...");
            });
    }

    render() {
        return (
            <SummaryList title={"Vos récentes plaintes"} items={this.state?.complains} />
        );
    }
}

export default ComplainsPage;