import React, {Component} from "react";
import {Route} from "react-router-dom";
import withRouteMatch from "../../utils/withRouteMatch";
import {routes} from "../../_config/routes";
import ComplainsPage from "./ComplainsPage";

class ComplainsHandler extends Component {
    constructor(props) {
        super(props);
        this.subroutes = routes.find(c => c.path === this.props.url)?.subroutes;
    }

    render() {
        return (
            <>
                <Route
                    exact={true}
                    path={"/complains"}
                >
                    <ComplainsPage />
                </Route>

                {
                    this.subroutes
                        ? this.subroutes.map(c =>
                            (
                                <Route
                                    key={`${this.props.url}/${c.path}`}
                                    path={`${this.props.url}/${c.path}`}
                                    exact={true}
                                >
                                    <c.component />
                                </Route>
                            )
                        )
                        : null
                }
            </>
        );
    }
}

export default withRouteMatch(ComplainsHandler);