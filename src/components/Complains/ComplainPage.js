import React from "react";
import {Badge, Card, Col, ListGroup, ListGroupItem, Row} from "react-bootstrap";
import backend from "../../_config/backend";
import {MediumFiveSpinner} from "../FiveSpinner";
import PropTypes from "prop-types";
import SummaryList from "../SummaryList";
import withId from "../../utils/withId";
import getGenderName from "../../utils/gender";
import {toast} from "react-toastify";
import {Link, Redirect} from "react-router-dom";
import withAuth from "../../utils/withAuth";

class ComplainPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.canDelete = this.props.data.perms["complain.delete"];
    }

    componentDidMount() {
        let id = this.props.id;

        if (isNaN(id))
        {
            toast.dark("❌ Plainte incorrect");
            this.setState({redirectTo: "/dashboard"});
            return;
        }

        Promise.all([
            backend.complain(id),
            backend.complain_identities(id)
        ])
            .then(([ complain, identities ]) => {
                this.setState({...complain, identities: identities.rows, identities_count: identities.count});
            })
            .catch(() => {
                toast.dark("❌ Une erreur s'est produite, veuillez ré-essayer plus tard");
                this.setState({redirectTo:"/dashboard"});
            });
    }

    deleteComplain() {
        backend.complain_delete(this.props.id)
            .then(() => {
                toast.dark("🤘 Plainte supprimée avec succès");
                this.setState({redirectTo: "/complains"});
            })
            .catch(() => toast.dark("❌ Une erreur s'est produite, impossible de supprimer"));
    }

    render_loading() {
        return (
            <Row>
                <Col lg>
                    <Card style={{width: "100%"}}>
                        <Card.Header className={"text-justify"}>
                            <MediumFiveSpinner />
                        </Card.Header>
                    </Card>
                </Col>
            </Row>
        );
    }

    formatForList(complain_identity) {
        return {
            head: complain_identity.full_name,
            body: getGenderName(complain_identity.gender),
            link: "/identities/" + complain_identity.identity_id,
            id: complain_identity.identity_id
        };
    }

    render() {
        if (this.state.redirectTo)
            return <Redirect to={this.state.redirectTo} />;

        if (!this.state.object)
            return this.render_loading();

        return (
            <>
                <Row>
                    <Col md={"12"} lg={6} sm={"12"}>
                        <Card className={"mt-2"}>
                            <Card.Body>
                                <Card.Title>{this.state.object}</Card.Title>
                                <Card.Subtitle className={"mb-2 text-muted"}>{this.state.description}</Card.Subtitle>
                                <Card.Text>

                                </Card.Text>
                                <Card.Footer className={"text-muted"}>Ajoutée le {new Date(this.state.createdAt).toLocaleDateString()}</Card.Footer>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={"12"} lg={6} sm={"12"}>
                        <Card className={"mt-2"}>
                            <Card.Body>
                                <ListGroup variant={"flush"}>
                                    <ListGroupItem>
                                        <Row>
                                            <Col>
                                                Nombre de plaignants
                                            </Col>
                                            <Col>
                                                {this.state.identities.filter(c => c.type === "complainer").length}
                                            </Col>
                                        </Row>
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <Row>
                                            <Col>
                                                Nombre d'accusés
                                            </Col>
                                            <Col>
                                                {this.state.identities.filter(c => c.type === "target").length}
                                            </Col>
                                        </Row>
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <Row>
                                            <Col>
                                                Actions
                                            </Col>
                                            <Col>
                                                <Link to={`${this.props.id}/edit`}>
                                                    <Badge className="text-right pull-right" variant="secondary">
                                                        Modifier
                                                    </Badge>
                                                </Link>
                                                {
                                                    this.canDelete &&
                                                    <Badge onClick={this.deleteComplain.bind(this)} className="text-right pull-right" variant="danger">
                                                        Supprimer
                                                    </Badge>
                                                }
                                            </Col>
                                        </Row>
                                    </ListGroupItem>
                                </ListGroup>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
                <Row className={"mt-2"}>
                    <Col lg={6} md={6} sm={12}>
                        <Card className={"mt-2"}>
                            <Card.Body>
                                {
                                    <SummaryList
                                        title={"Plaignant(s)"}
                                        items={this.state.identities.filter(c => c.type === "complainer").map(identity_complain => this.formatForList(identity_complain.identity))}
                                    />
                                }
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col lg={6}  md={6} sm={12}>
                        <Card className={"mt-2"}>
                            <Card.Body>
                                {
                                    <SummaryList
                                        title={"Accusés"}
                                        items={this.state.identities.filter(c => c.type === "target").map(identity_complain => this.formatForList(identity_complain.identity))}
                                    />
                                }
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </>
        );
    }
}

ComplainPage.propTypes = {
    id: PropTypes.number.isRequired
};

export default withId(withAuth(ComplainPage));