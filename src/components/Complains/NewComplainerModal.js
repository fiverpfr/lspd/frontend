import React, {Component} from "react";
import {Badge, Button, Card, ListGroup, ListGroupItem, Modal} from "react-bootstrap";
import {IdentitiesSearchForm} from "../Identities/IdentitiesSearch";
import backend from "../../_config/backend";
import {formatIdentityWithComponent} from "../../utils/identity";
import SummaryList from "../SummaryList";
import {toast} from "react-toastify";
import PropTypes from "prop-types";
import getGenderName from "../../utils/gender";
import {formatFullDate} from "../../utils/date";

class NewComplainerModal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            searchValue: "",
            identity: undefined
        };

        this.setShow = this.setShow.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.show && !prevState.show)
            this.setState({searchValue: "", identity: undefined, identities: undefined});
    }

    addIdentity(type) {
        if (this.props.onAddIdentity(this.state.identity, type))
            this.setShow(false);
    }

    onValueChange(e) {
        let { value } = e.target;
        this.setState({searchValue: value});

        if (value.length > 3)
            this.searchIdentities(value);
    }

    resetSearch() {
        this.setState({identity: undefined, identities: null, searchValue: ""});
    }

    searchIdentities(query) {
        backend.identities_search(query, 5)
            .then(result => {
                result.rows = result.rows.map(c => formatIdentityWithComponent(
                    c,
                    <div>
                        <Badge variant="primary" onClick={() => this.setState({identity: c})}>Sélectionner</Badge>
                    </div>
                ));

                this.setState({ identities: result.rows });
            })
            .catch(() => {
                toast.dark("❌ Impossible de récupérer la liste des identités...");
            });
    }

    showForm() {
        return (
            <>
                <IdentitiesSearchForm value={this.state.searchValue} onChange={this.onValueChange} />
                {
                    this.state.identities?.length > 0
                        ? <SummaryList title={"Identités trouvées"} items={this.state.identities} />
                        : null
                }
            </>
        );
    }

    showTypeSelect() {
        let { full_name, gender, phone_number, createdAt } = this.state.identity;
        return (
            <Card>
                <Card.Body>
                    <Card.Title>{full_name}</Card.Title>
                </Card.Body>
                <ListGroup className="list-group-flush">
                    <ListGroupItem>Genre : {getGenderName(gender)}</ListGroupItem>
                    <ListGroupItem>Numéro : {phone_number}</ListGroupItem>
                    <ListGroupItem>Date d'ajout : {formatFullDate(createdAt)}</ListGroupItem>
                </ListGroup>
                <Card.Body>
                    <Card.Link onClick={() => this.addIdentity("complainer") }>Ajouter en plaignant</Card.Link>
                    <Card.Link onClick={() => this.addIdentity("target") }>Ajouter en accusé</Card.Link>
                </Card.Body>
            </Card>
        );
    }

    setShow(value) {
        this.setState({show: value});
    }

    render() {
        return (
            <>
                <Button variant="primary" onClick={() => this.setShow(true)} size="lg" block>
                    Lier une identité à la plainte
                </Button>
                <Modal
                    show={this.state.show}
                    onHide={() => this.setShow(false)}
                    dialogClassName="modal-90w"
                    aria-labelledby="example-custom-modal-styling-title"
                    animation={false}
                >
                    <Modal.Header closeButton>
                        <Modal.Title>Lier une nouvelle identité</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        {
                            !this.state.identity
                                ? this.showForm()
                                : this.showTypeSelect()
                        }
                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="secondary" onClick={() => this.state.identity ? this.resetSearch() : this.setShow(false)}>Retour</Button>
                    </Modal.Footer>
                </Modal>
            </>
        );
    }
}

NewComplainerModal.propTypes = {
    onAddIdentity: PropTypes.func.isRequired
};

export default NewComplainerModal;