import React, {Component} from "react";
import {toast} from "react-toastify";
import backend from "../../_config/backend";
import withId from "../../utils/withId";
import {Redirect} from "react-router-dom";
import UserForm from "./UserForm";
import Loading from "../Loading";
import * as yup from "yup";


class UserEdit extends Component {
    validationSchema = yup.object({
        firstName: yup.string()
            .required("Prénom nécessaire"),
        lastName: yup.string()
            .required("Nom nécessaire"),
        password: yup.string(),
        primaryRank: yup.string()
            .required("Le rang est nécessaire"),
        ranks: yup.array()
    });

    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount() {
        let { id } = this.props;

        Promise.all([
            backend.user(id),
            backend.ranks()
        ])
            .then(([user, ranks]) => {
                this.setState({
                    rank_list: ranks.rows,
                    user: {
                        firstName: user.first_name,
                        lastName: user.last_name,
                        password: "",
                        primaryRank: user.primary_rank.rankId,
                        ranks: user.ranks.filter(c => c.rankId !== user.primary_rank.rankId).map(c => c.rankId)
                    }
                });
            })
            .catch(() => {
                toast.dark("❌ Cet utilisateur n'existe pas ou est incorrect");
                this.setState({redirectTo: "/users"});
            });

    }

    async onSubmit(args, functions) {
        const {
            primaryRank,
            ranks
        } = args;

        const {
            setSubmitting
        } = functions;

        try {
            await this.updateUserProfile(this.props.id, args);
            await this.updateUserRanks(this.props.id, primaryRank, ranks);
            this.setState({redirectTo: "/users"});
            toast.dark("🚀 Utilisateur mis à jour");
        }
        catch {
            toast.dark("❌ Une erreur s'est produite, veuillez ré-essayer plus tard");
            setSubmitting(false);
        }
    }

    /**
     * Update main profile data (ranks not included)
     * @param {number} userId
     * @param {string} firstName
     * @param {string} lastName
     * @param {string} password
     * @returns {Promise<boolean>} request is successful
     */
    updateUserProfile(userId, {firstName, lastName, password}) {
        const {firstName: stateFirstName, lastName: stateLastName} = this.state.user;

        if (firstName === stateFirstName && lastName === stateLastName && password.length === 0)
            return Promise.resolve(true);

        return new Promise((res, rej) => {
            backend.user_update(userId, {
                first_name: firstName,
                last_name: lastName,
                password: password
            })
                .then(() => res(true))
                .catch(err => rej(err));
        });
    }

    /**
     * Update a profile ranks
     * @param {number} userId
     * @param {string} primaryRank
     * @param {Array<string>} ranks
     */
    updateUserRanks(userId, primaryRank, ranks) {
        const {primaryRank: statePrimaryRank, ranks: stateRanks} = this.state.user;

        if (primaryRank === statePrimaryRank &&
            !ranks.find(c => stateRanks.indexOf(c) === -1) &&
            !stateRanks.find(c => ranks.indexOf(c) === -1)
        )
            return Promise.resolve(true);

        return new Promise((res, rej) => {
            Promise.all(
                stateRanks
                    .filter(rank => ranks.indexOf(rank) === -1)
                    .map(rank => backend.user_remove_rank(userId, {rankId: rank, primary: false})),
                ranks
                    .filter(rank => stateRanks.indexOf(rank) === -1)
                    .map(rank => backend.user_rank(userId, {rankId: rank, primary: false}))
            )
                .then(() => res(true))
                .catch(err => rej(err));

            if (primaryRank !== statePrimaryRank)
            {
                backend.user_rank(userId, {rankId: primaryRank, primary: true})
                    .then(() => res(true))
                    .catch(err => rej(err));
            }
        });
    }

    render() {
        if (this.state?.redirectTo)
            return <Redirect to={this.state.redirectTo} />;


        return (
            <>
                <Loading isActive={this.state?.user === null} />
                {
                    this.state?.user
                    && <UserForm onSubmit={this.onSubmit} validationSchema={this.validationSchema} initialValues={this.state.user} rankList={this.state.rank_list} />
                }
            </>
        );
    }
}

export default withId(UserEdit);