import * as yup from "yup";
import {useFormik} from "formik";
import {Button, Col, Form} from "react-bootstrap";
import {SmallFiveSpinner} from "../FiveSpinner";
import React from "react";

const schema = yup.object({
    firstName: yup.string()
        .required("Prénom nécessaire"),
    lastName: yup.string()
        .required("Nom nécessaire"),
    password: yup.string()
        .required("Mot de passe nécessaire"),
    primaryRank: yup.string()
        .required("Le rang est nécessaire"),
    ranks: yup.array()
});

const UserForm = ({initialValues, onSubmit, rankList, validationSchema}) => {
    const formik = useFormik({
        initialValues: initialValues || {
            firstName: "",
            lastName: "",
            password: ""
        },
        onSubmit: onSubmit,
        validationSchema: validationSchema || schema
    });
    const { values, handleChange, errors, handleSubmit, isSubmitting } = formik;

    return (
        <Form noValidate onSubmit={handleSubmit}>
            <Form.Row>
                <Form.Group as={Col} sm={12} md={6} lg={6} controlId={"validationFirstName"}>
                    <Form.Label>Prénom</Form.Label>
                    <Form.Control
                        type="text"
                        name={"firstName"}
                        value={values.firstName}
                        onChange={handleChange}
                        isInvalid={errors.firstName}
                        placeholder="Prénom"
                    />
                    <Form.Control.Feedback type={"invalid"}>
                        {errors.firstName}
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group as={Col} sm={12} md={6} lg={6}  controlId={"validationLastName"}>
                    <Form.Label>Nom</Form.Label>
                    <Form.Control
                        type="text"
                        name={"lastName"}
                        value={values.lastName}
                        onChange={handleChange}
                        isInvalid={errors.lastName}
                        placeholder="Nom"
                    />
                    <Form.Control.Feedback type={"invalid"}>
                        {errors.lastName}
                    </Form.Control.Feedback>
                </Form.Group>
            </Form.Row>
            <Form.Row>
                <Form.Group as={Col} sm={12} md={12} lg={12}  controlId={"validationPrimaryRank"}>
                    <Form.Label>Rang primaire</Form.Label>
                    <Form.Control
                        as={"select"}
                        name={"primaryRank"}
                        onChange={handleChange}
                        value={values.primaryRank}
                        isInvalid={errors.primaryRank}
                    >
                        {
                            rankList.map(c =>
                                <option key={c.rankId} value={c.rankId}>{c.name}</option>
                            )
                        }
                    </Form.Control>
                    <Form.Control.Feedback type={"invalid"}>
                        {errors.primaryRank}
                    </Form.Control.Feedback>
                </Form.Group>
            </Form.Row>
            <Form.Row>
                <Form.Group as={Col} sm={12} md={12} lg={12}  controlId={"validationRank"}>
                    <Form.Label>Autres rangs</Form.Label>
                    <Form.Control
                        as={"select"}
                        multiple
                        name={"ranks"}
                        onChange={handleChange}
                        value={values.ranks}
                        isInvalid={errors.ranks}
                    >
                        {
                            rankList.map(c =>
                                values.primaryRank !== c.rankId
                                    ? <option key={c.rankId} value={c.rankId}>{c.name}</option>
                                    : null
                            )
                        }
                    </Form.Control>
                    <Form.Control.Feedback type={"invalid"}>
                        {errors.ranks}
                    </Form.Control.Feedback>
                </Form.Group>
            </Form.Row>
            <Form.Row>
                <Form.Group as={Col} sm={12} md={12} lg={12}  controlId={"validationPassword"}>
                    <Form.Label>Mot de passe</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Mon super mot de passe"
                        name={"password"}
                        onChange={handleChange}
                        value={values.password}
                        isInvalid={errors.password}
                    />
                    <Form.Control.Feedback type={"invalid"}>
                        {errors.password}
                    </Form.Control.Feedback>
                </Form.Group>
            </Form.Row>
            <Button disabled={isSubmitting} variant={"primary"} type={"submit"}>
                {isSubmitting ? <SmallFiveSpinner /> : "Valider"}
            </Button>
        </Form>
    );
};

export default UserForm;