import React from "react";
import backend from "../../_config/backend";
import {toast} from "react-toastify";
import {Button, Row} from "react-bootstrap";
import UsersTable from "./UsersTable";
import withAuth from "../../utils/withAuth";
import ToggleAction from "../ToggleAction";
import {Link} from "react-router-dom";

const USER_PER_PAGE = 10;

function UserRow({user, canToggleUser, canUpdateUser, onChange}) {
    return (
        <tr>
            <td>{user.full_name}</td>
            <td>{user.primary_rank?.name || "Aucun"}</td>
            <td>
                {
                    canToggleUser
                        ? <ToggleAction id={user.id.toString()} onChange={onChange} current={user.active} />
                        : user.active ? "✅" : "❌"
                }
            </td>
            <td>
                {
                    canUpdateUser
                        ?
                        <Link to={`/users/${user.id}/edit`}>
                            <Button variant="primary" size="sm">
                                Mettre à jour
                            </Button>
                        </Link>
                        : <p>Aucune action disponible</p>
                }
            </td>
        </tr>
    );
}

class UsersPage extends React.Component {
    constructor(props) {
        super(props);
        this.user_data = props.data;
        this.state = {
            user_count: 0,
            users: []
        };
    }

    componentDidMount() {
        this.handlePageUpdate(0);
    }

    handlePageUpdate(offset) {
        Promise.all([
            backend.users(USER_PER_PAGE, offset),
            backend.users_count()
        ])
            .then(([users, count]) => {
                this.setState({user_count: count, users: users.rows});
            })
            .catch(() => {
                toast.dark("❌ Impossible de charger les utilisateurs");
                this.setState({redirectTo: "dashboard"});
            });
    }

    /**
     * Active or deactive the player
     * @param user
     */
    handleUserToggle(user) {
        backend.user_update(user.id, {
            active: !user.active
        })
            .then(() => {
                let users = this.state.users;

                users.find(c => c.id === user.id).active = !user.active;
                this.setState({users: users});
                toast.dark("✅ Utilisateur mis à jour");
                return true;
            })
            .catch(() => {
                toast.dark("❌ Impossible de mettre à jour l'utilisateur tout de suite");
                return false;
            });
    }

    render() {
        let { user_count, users } = this.state;
        const { "user.toggle": canToggleUser, "user.update": canUpdateUser, canCreateUser } = this.user_data.perms;

        users = users.map(c => <UserRow
            key={c.id}
            user={c}
            canToggleUser={canToggleUser}
            canUpdateUser={canUpdateUser}
            canCreateUser={canCreateUser}
            onChange={() => this.handleUserToggle(c)}
        />);

        return (
            <Row>
                <h1 className={"mb-5"}>Liste des utilisateurs</h1>
                <UsersTable
                    itemsCount={user_count}
                    itemsPerPage={USER_PER_PAGE}
                    items={users}
                    handlePageChange={this.handlePageUpdate.bind(this)}
                />
            </Row>
        );
    }
}

export default withAuth(UsersPage);