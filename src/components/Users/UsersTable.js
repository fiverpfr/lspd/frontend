import React, {Component} from "react";
import {Pagination, Table} from "react-bootstrap";
import PropTypes from "prop-types";
import { MediumFiveSpinner} from "../FiveSpinner";

class UsersTable extends Component {
    constructor(props) {
        super(props);

        this.state = {
            current_page: 1
        };
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.loading_items && this.props.items.length > 0)
            setTimeout(() => this.setState({loading_items: false}), 200);
    }

    getPagesCount() {
        return Math.ceil(this.props.itemsCount / this.props.itemsPerPage);
    }

    getMidPagesArray() {
        if (this.state.current_page === 1 && this.props.items.length > this.props.itemsPerPage)
            return [
                this.state.current_page,
                this.state.current_page + 1,
            ];
        else if (this.state.current_page === this.getPagesCount() && this.props.items.length > this.props.itemsPerPage)
            return [
                this.state.current_page - 1,
                this.state.current_page,
            ];
        else if (this.state.current_page === 1)
            return [
                this.state.current_page
            ];

        return [
            this.state.current_page - 1,
            this.state.current_page,
            this.state.current_page + 1,
        ];
    }

    getLeftButtons() {
        const { current_page } = this.state;
        return (
            <>
                <Pagination.First
                    onClick={() => this.handlePageClick(1)}
                    disabled={current_page === 1}
                />
                <Pagination.Prev
                    onClick={() => this.handlePageClick(this.state.current_page - 1)}
                    disabled={current_page === 1}
                />
            </>
        );
    }

    getRightButtons() {
        const { current_page } = this.state;

        return (
            <>
                <Pagination.Next
                    onClick={() => this.handlePageClick(this.state.current_page + 1)}
                    disabled={current_page >= this.getPagesCount() - 1}
                />
                <Pagination.Last
                    onClick={() => this.handlePageClick(this.getPagesCount())}
                    disabled={current_page >=  this.getPagesCount() - 1}
                />
            </>
        );
    }

    handlePageClick(number) {
        this.props.handlePageChange((number-1) * this.props.itemsPerPage);
        this.setState({current_page: number, loading_items: true});
    }

    render() {
        const { items } = this.props;
        const { current_page, loading_items } = this.state;

        return (
            <>
                <Table striped bordered hover responsive>
                    <thead>
                        <tr>
                            <th>Nom d'utilisateur</th>
                            <th>Rang primaire</th>
                            <th>Actif</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            !loading_items
                                ? items
                                : <tr>
                                    <td>...</td>
                                    <td><MediumFiveSpinner /></td>
                                    <td>...</td>
                                    <td>...</td>
                                </tr>
                        }
                    </tbody>
                </Table>
                <Pagination>
                    {
                        this.getLeftButtons()
                    }
                    {
                        this.getPagesCount() > 3 && current_page > 2
                            ? <Pagination.Ellipsis />
                            : null
                    }

                    {
                        this.getMidPagesArray().map(c => (
                            <Pagination.Item
                                onClick={() => this.handlePageClick(c)}
                                key={c}
                                active={current_page === c}
                            >
                                {c}
                            </Pagination.Item>
                        ))
                    }

                    {
                        this.getPagesCount() > 3 && current_page < this.getPagesCount() - 1
                            ? <Pagination.Ellipsis />
                            : null
                    }

                    {
                        this.getRightButtons()
                    }
                </Pagination>
            </>
        );
    }
}

UsersTable.propTypes = {
    itemsPerPage: PropTypes.number.isRequired,
    itemsCount: PropTypes.number.isRequired,
    items: PropTypes.array.isRequired,
    handlePageChange: PropTypes.func.isRequired
};

export default UsersTable;