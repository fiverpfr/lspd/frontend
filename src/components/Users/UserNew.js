import React, {Component} from "react";
import {Redirect} from "react-router-dom";
import withAuth from "../../utils/withAuth";
import {toast} from "react-toastify";
import UserForm from "./UserForm";
import backend from "../../_config/backend";

class UserNew extends Component {
    constructor(props) {
        super(props);

        if (!props.data.perms["user.create"])
        {
            this.state = {redirectTo: "/dashboard"};
            toast.dark("❌ Vous n'avez pas accès à cette zone");
            return;
        }

        this.state = {
            rank_list: []
        };
    }

    componentDidMount() {
        backend.ranks()
            .then(result => {
                this.setState({rank_list: result.rows});
            })
            .catch(() => {
                toast.dark("❌ Impossible de récupérer la liste des rangs, ré-essayez plus tard");
                this.setState({redirectTo: "/dashboard"});
            });
    }

    onSubmit(args) {
        const {
            firstName,
            lastName,
            password,
            primaryRank,
            ranks
        } = args;

        backend.user_new({
            first_name: firstName,
            last_name: lastName,
            password: password,
            social_id: 999
        })
            .then(async user_data => {
                await backend.user_rank(user_data.id, {rankId: primaryRank, primary: true});

                if (ranks !== undefined) {
                    await Promise.all(ranks.map(c => backend.user_rank(user_data.id, {rankId: c, primary: false})));
                }

                toast.dark("✅ Nouvel utilisateur créé");
                this.setState({redirectTo: "/users"});
            })
            .catch(() => {
                toast.dark("❌ Une erreur s'est produite, veuillez ré-essayer");
                this.setState({redirectTo: "/users"});
            });
    }

    render() {
        if (this.state?.redirectTo)
            return <Redirect to={this.state.redirectTo} />;

        return (
            <UserForm onSubmit={this.onSubmit.bind(this)} rankList={this.state.rank_list} />
        );
    }
}

export default withAuth(UserNew);