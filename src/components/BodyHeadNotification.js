import React, {Component} from "react";
import fiverpLogo from "../images/logo.svg";
import PropTypes from "prop-types";

class BodyHeadNotificationClass extends Component {
    render() {
        return (
            <div className="d-flex align-items-center p-3 my-3 text-white bg-purple rounded shadow-sm">
                <img className="me-3" src={fiverpLogo} alt="FiveRP Logo" width="48" height="38" />
                <div className="lh-1">
                    <h1 className="h6 mb-0 text-white lh-1">{this.props.text}</h1>
                    <small>{this.props.subText}</small>
                </div>
            </div>
        );
    }
}

BodyHeadNotificationClass.propTypes = {
    subText: PropTypes.string,
    text: PropTypes.string
};

export default BodyHeadNotificationClass;