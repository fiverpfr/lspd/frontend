import React, {Component} from "react";
import LoginForm from "../LoginForm";
import lspdLogo from "../../images/lspd.png";
import {Alert} from "react-bootstrap";
import {Redirect} from "react-router-dom";
import withAuth from "../../utils/withAuth";
import Container from "./styled";

class LoginPage extends Component {
    state = {
        loading: false,
        message: {
            variant: "danger",
            content: null
        }
    }

    initialValues = {
        username: "",
        password: ""
    }

    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
        this.clearError = this.clearError.bind(this);
        this.validateForm = this.validateForm.bind(this);
        this.actions = props.actions;
        this.ref = React.createRef();
    }

    clearError() {
        this.setState({
            message: { content: null }
        });
    }

    handleSubmit({username, password}) {
        let error = "";
        let [ first_name, last_name ] = username.split("_");

        if (first_name === undefined || last_name === undefined || first_name.length <= 3 || last_name.length <= 3)
            error = "Format du nom d'utilisateur incorrect";
        else if (password.length <= 3)
            error = "Format du mot de passe incorrect";

        if (error)
        {
            this.setState({message: {content: error, variant: "danger"}});
            return;
        }

        this.setState({loading: true});
        this.actions.onLogin(username, password)
            .then(() => this.setState({redirectTo: "/"}))
            .catch(() => this.setState({message: {content: "Nom d'utilisateur ou mot de passe inconnu", variant: "danger"}}));

    }

    validateForm({username}) {
        let errors = {};
        if (username.length > 7) {
            let cut = username.split("_");

            if (cut.length === 1)
                errors.username = "Format: John_Doe";
        }

        return errors;
    }

    loggedOutRender() {
        return (
            <Container>
                <div className="sidenav">
                    <div className="login-main-text">
                        <img src={lspdLogo} alt="Logo LSPD"/>
                        <h2><br /> Se connecter</h2>
                        <p>C'est une zone avec un accès restreint.</p>
                        <p>Les conditions générales d'utilisation de FiveRP s'appliquent sur ce site.</p>
                    </div>
                </div>
                <div className="main">
                    <div className="col-md-6 col-sm-12">
                        <div className="login-form">
                            <h3>Connexion</h3>
                            {
                                <Alert variant={this.state.message.variant} show={this.state.message.content != null} transition={null} onClose={this.clearError} dismissible>
                                    {this.state.message.content}
                                </Alert>
                            }

                            {
                                <LoginForm
                                    onSubmit={this.handleSubmit}
                                    initialValues={this.initialValues}
                                    validate={this.validateForm}
                                    loadi ng={this.state.loading}
                                />
                            }
                        </div>
                    </div>
                </div>
            </Container>
        );
    }

    render() {
        return (
            this.actions.isLoggedIn()
                ? <Redirect to={"/"} />
                : this.loggedOutRender()
        );
    }
}

export default withAuth(LoginPage);
