import fiveLoading from "../images/five_loading.gif";
import React from "react";

export function SmallFiveSpinner(props) {
    const { width, height } = props;
    return (
        <img width={width ?? "32px"} height={height ?? "32px"} src={fiveLoading} alt="FiveRP Spinner" />
    );
}

export function MediumFiveSpinner(props) {
    const { width, height } = props;
    return <SmallFiveSpinner width={width ?? "64px"} height={height ?? "64px"} />;
}

export function BigFiveSpinner(props) {
    const { width, height } = props;
    return <SmallFiveSpinner width={width ?? "128px"} height={height ?? "128px"} />;
}