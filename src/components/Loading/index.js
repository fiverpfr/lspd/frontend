import React from "react";
import fiveLoading from "../../images/five_loading.gif";
import styled from "styled-components";
import PropTypes from "prop-types";

const Loading = ({ text, isActive }) => {
    return (
        <Container isActive={isActive}>
            <img src={fiveLoading} alt="Chargement FiveRP" />
            <p>{text || "Chargement en cours..."}</p>
        </Container>
    );
};

Loading.propTypes = {
    text: PropTypes.string,
    isActive: PropTypes.bool.isRequired
};

const Container = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 100vh;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: black;
  transition: .5s;
  opacity: ${({ isActive }) => isActive ? "1" : "0"};
  z-index: ${({ isActive }) => isActive ? "999999" : "-1"};
  
  img {
    width: 10%;
  }

  p {
    font-family: 'Roboto',sans-serif;
    font-weight: bold;
    color: white;
  }
`;

export default Loading;
