import styled from "styled-components";

const Container = styled.div`
  body {
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
    sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
    monospace;
  }

  .fiverpLogo {
    margin-top: 15px;
  }

  .text-white-50 { color: rgba(255, 255, 255, .5); }

  .bg-purple { background-color: #6f42c1; }

  .me-2 { margin-right: .5rem;}    
`;

export default Container;