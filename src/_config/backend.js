class backend {
    _url = process.env.REACT_APP_BACKEND_URI || "http://localhost:3000"

    /**
     * Header for HTTP requests
     */
    _fetchHeader = {
        "content-type": "application/json",
        "Accept": "application/json"
    }

    /**
     * Header for HTTP requests with Bearer Token
     */
    get _fetchHeaderToken() {
        return {
            ...this._fetchHeader,
            "Authorization": "Bearer " + this.token,
        };
    }

    /**
     * Fetch token for current session in localStorage
     * @param token - New token
     * @returns {string} - Returns token if exists else null
     * @private
     */
    _token(token) {
        if (typeof token === "string")
            localStorage.setItem("token", token);

        return localStorage.getItem("token");
    }

    /**
     * Clear token in localStorage
     */
    clearToken() {
        localStorage.removeItem("token");
    }

    /**
     * Retrieve token
     * @returns {string} - Token
     * @returns {string|null}
     */
    get token() { return this._token(); }

    /**
     * Fetch if the current user has permission
     * @param {string} perm
     * @returns {Promise<boolean>}
     */
    can(perm) {
        if (this.token === undefined || this.token === null)
        {
            console.error("Cannot retrieve backend GET /users/me/can without a token");
            return;
        }

        return new Promise((res, rej) => {
            fetch(`${this._url}/users/me/can?perm=${encodeURI(perm)}`, {
                headers: {
                    ...this._fetchHeaderToken
                }
            })
                .then(async result => {
                    let response = await result.json();

                    if (!response.success)
                        res(false);

                    res(true);
                })
                .catch(() => rej(false));
        });
    }

    /**
     * Fetching user data
     */
    me() {
        if (this.token === undefined || this.token === null)
        {
            console.error("Cannot retrieve backend GET /users/me without a token");
            return;
        }

        return new Promise((res, rej) => {
            fetch(`${this._url}/users/me`, {
                headers: {
                    ...this._fetchHeaderToken
                }
            })
                .then(async result => {
                    let userData = await result.json();

                    if (result.status !== 200 || !userData.success)
                        rej(userData.message);

                    res(userData.data);
                })
                .catch(result => rej(result));
        });
    }

    /**
     * Fetching current user complains
     */
    me_complains() {
        if (this.token === undefined || this.token === null)
        {
            console.error("Cannot retrieve backend GET /users/me/complains without a token");
            return;
        }

        return new Promise((res, rej) => {
            fetch(`${this._url}/users/me/complains`, {
                headers: {
                    ...this._fetchHeaderToken
                }
            })
                .then(async result => {
                    let complains = await result.json();

                    if (result.status !== 200 || !complains.success)
                        rej(complains.message);

                    res(complains.data);
                })
                .catch(result => rej(result));
        });
    }

    /**
     * Log in the user
     * @param {string} username
     * @param {string} password
     * @returns {Promise<unknown>}
     */
    login({username, password}) {
        return new Promise((res, rej) => {
            fetch(`${this._url}/auth/login`, {
                method: "POST",
                body: JSON.stringify({username, password}),
                headers: {
                    ...this._fetchHeader
                }
            })
                .then(async result => {
                    let data = await result.json();

                    if (result.status === 401 || !data.success)
                        res(false);

                    return data.data;
                })
                .then(token => {
                    this._token(token);
                    res(true);
                })
                .catch(err => rej("Une erreur s'est produite: " + err));
        });
    }

    health() {
        return new Promise((res, rej) => {
            fetch(`${this._url}/health`, {
                headers: {
                    ...this._fetchHeader
                }
            })
                .then(async result => {
                    let data = await result.json();

                    if (!data.data.healthy)
                        rej(data);
                    else
                        res();

                })
                .catch(() => rej("Le service support n'est plus accessible, veuillez nous excuser pour la gêne"));
        });
    }

    /**
     * Fetching users
     */
    users(limit = 15, offset = 0) {
        if (this.token === undefined || this.token === null)
        {
            console.error("Cannot retrieve backend GET /users/ without a token");
            return;
        }

        return new Promise((res, rej) => {
            fetch(`${this._url}/users?limit=${limit}&offset=${offset}`, {
                headers: {
                    ...this._fetchHeaderToken
                }
            })
                .then(async result => {
                    let users = await result.json();

                    if (result.status !== 200 || !users.success)
                        rej(users.message);

                    res(users.data);
                })
                .catch(result => rej(result));
        });
    }

    /**
     * Fetching ranks
     */
    ranks() {
        if (this.token === undefined || this.token === null)
        {
            console.error("Cannot retrieve backend GET /ranks/ without a token");
            return;
        }

        return new Promise((res, rej) => {
            fetch(`${this._url}/ranks`, {
                headers: {
                    ...this._fetchHeaderToken
                }
            })
                .then(async result => {
                    let ranks = await result.json();

                    if (result.status !== 200 || !ranks.success)
                        rej(ranks.message);

                    res(ranks.data);
                })
                .catch(result => rej(result));
        });
    }

    /**
     * Fetching user
     */
    user(id) {
        if (this.token === undefined || this.token === null)
        {
            console.error("Cannot retrieve backend GET /user/:id without a token");
            return;
        }

        return new Promise((res, rej) => {
            fetch(`${this._url}/users/${id}`, {
                headers: {
                    ...this._fetchHeaderToken
                }
            })
                .then(async result => {
                    let user = await result.json();

                    if (result.status !== 200 || !user.success)
                        rej(user.message);

                    res(user.data);
                })
                .catch(result => rej(result));
        });
    }

    /**
     * Add rank to user
     */
    user_rank(id, data) {
        if (this.token === undefined || this.token === null)
        {
            console.error("Cannot retrieve backend POST /users/:id/rank without a token");
            return;
        }

        return new Promise((res, rej) => {
            fetch(`${this._url}/users/${id}/rank`, {
                method: "POST",
                body: JSON.stringify(data),
                headers: {
                    ...this._fetchHeaderToken,
                }
            })
                .then(async result => {
                    let user_data = await result.json();

                    if ([200, 201].indexOf(result.status) === -1 || !user_data.success)
                        rej(user_data.message);

                    res(user_data.data);
                })
                .catch(result => rej(result));
        });
    }

    /**
     * Remove rank to user
     */
    user_remove_rank(id, data) {
        if (this.token === undefined || this.token === null)
        {
            console.error("Cannot retrieve backend POST /users/:id/rank/:rankId without a token");
            return;
        }

        return new Promise((res, rej) => {
            fetch(`${this._url}/users/${id}/rank/${data.rankId}`, {
                method: "DELETE",
                headers: {
                    ...this._fetchHeaderToken,
                }
            })
                .then(async result => {
                    let rank_data = await result.json();

                    if ([200, 201].indexOf(result.status) === -1 || !rank_data.success)
                        rej(rank_data.message);

                    res(rank_data.data);
                })
                .catch(result => rej(result));
        });
    }

    /**
     * Fetching users count
     */
    users_count() {
        if (this.token === undefined || this.token === null)
        {
            console.error("Cannot retrieve backend GET /users/count without a token");
            return;
        }

        return new Promise((res, rej) => {
            fetch(`${this._url}/users/count`, {
                headers: {
                    ...this._fetchHeaderToken
                }
            })
                .then(async result => {
                    let users = await result.json();

                    if (result.status !== 200 || !users.success)
                        rej(users.message);

                    res(users.data);
                })
                .catch(result => rej(result));
        });
    }

    /**
     * Update user
     */
    user_update(id, data) {
        if (this.token === undefined || this.token === null)
        {
            console.error("Cannot retrieve backend PUT /users/:id without a token");
            return;
        }

        return new Promise((res, rej) => {
            fetch(`${this._url}/users/${id}`, {
                method: "PUT",
                body: JSON.stringify(data),
                headers: {
                    ...this._fetchHeaderToken,
                }
            })
                .then(async result => {
                    let user_data = await result.json();

                    if ([200, 201].indexOf(result.status) === -1 || !user_data.success)
                        rej(user_data.message);

                    res(user_data.data);
                })
                .catch(result => rej(result));
        });
    }

    /**
     * Creating a new user
     */
    user_new(data) {
        if (this.token === undefined || this.token === null)
        {
            console.error("Cannot retrieve backend POST /users without a token");
            return;
        }

        return new Promise((res, rej) => {
            fetch(`${this._url}/users`, {
                method: "POST",
                body: JSON.stringify(data),
                headers: {
                    ...this._fetchHeaderToken,
                }
            })
                .then(async result => {
                    let user_data = await result.json();

                    if ([200, 201].indexOf(result.status) === -1 || !user_data.success)
                        rej(user_data.message);

                    res(user_data.data);
                })
                .catch(result => rej(result));
        });
    }

    /**
     * Fetching identities
     */
    identities() {
        if (this.token === undefined || this.token === null)
        {
            console.error("Cannot retrieve backend GET /identities/ without a token");
            return;
        }

        return new Promise((res, rej) => {
            fetch(`${this._url}/identities`, {
                headers: {
                    ...this._fetchHeaderToken
                }
            })
                .then(async result => {
                    let identities = await result.json();

                    if (result.status !== 200 || !identities.success)
                        rej(identities.message);

                    res(identities.data);
                })
                .catch(result => rej(result));
        });
    }

    /**
     * Fetching identity
     */
    identity(id) {
        if (this.token === undefined || this.token === null)
        {
            console.error("Cannot retrieve backend GET /identities/:id without a token");
            return;
        }

        return new Promise((res, rej) => {
            fetch(`${this._url}/identities/${id}`, {
                headers: {
                    ...this._fetchHeaderToken
                }
            })
                .then(async result => {
                    let identity = await result.json();

                    if (result.status !== 200 || !identity.success)
                        rej(identity.message);

                    res(identity.data);
                })
                .catch(result => rej(result));
        });
    }

    /**
     * Fetching identity complains
     */
    identity_complains(id) {
        if (this.token === undefined || this.token === null)
        {
            console.error("Cannot retrieve backend GET /identities/:id/complains without a token");
            return;
        }

        return new Promise((res, rej) => {
            fetch(`${this._url}/identities/${id}/complains`, {
                headers: {
                    ...this._fetchHeaderToken
                }
            })
                .then(async result => {
                    let identity = await result.json();

                    if (result.status !== 200 || !identity.success)
                        rej(identity.message);

                    res(identity.data);
                })
                .catch(result => rej(result));
        });
    }

    /**
     * Fetching identities
     */
    identities_search(query, limit = 15, offset = 0) {
        if (this.token === undefined || this.token === null)
        {
            console.error("Cannot retrieve backend GET /identities/search without a token");
            return;
        }
        let url = `${this._url}/identities/search?q=${encodeURI(query)}&limit=${limit}&offset=${offset}`;

        return new Promise((res, rej) => {
            fetch(url, {
                headers: {
                    ...this._fetchHeaderToken
                }
            })
                .then(async result => {
                    let identities = await result.json();

                    if (result.status !== 200 || !identities.success)
                        rej(identities.message);

                    res(identities.data);
                })
                .catch(result => rej(result));
        });
    }

    /**
     * Creating a new identity
     */
    identity_new(data) {
        if (this.token === undefined || this.token === null)
        {
            console.error("Cannot retrieve backend GET /identities/search without a token");
            return;
        }

        return new Promise((res, rej) => {
            fetch(`${this._url}/identities`, {
                method: "POST",
                body: JSON.stringify(data),
                headers: {
                    ...this._fetchHeaderToken,
                }
            })
                .then(async result => {
                    let identity = await result.json();

                    if ([200, 201].indexOf(result.status) === -1 || !identity.success)
                        rej(identity.message);

                    res(identity.data);
                })
                .catch(result => rej(result));
        });
    }

    /**
     * Update an existing identity
     */
    identity_update(id, data) {
        if (this.token === undefined || this.token === null)
        {
            console.error("Cannot retrieve backend PUT /identities/:id without a token");
            return;
        }

        return new Promise((res, rej) => {
            fetch(`${this._url}/identities/${id}`, {
                method: "PUT",
                body: JSON.stringify(data),
                headers: {
                    ...this._fetchHeaderToken,
                }
            })
                .then(async result => {
                    let identity = await result.json();

                    if ([200, 201].indexOf(result.status) === -1 || !identity.success)
                        rej(identity.message);

                    res(identity.data);
                })
                .catch(result => rej(result));
        });
    }

    /**
     * Fetching complain
     */
    complain(id) {
        if (this.token === undefined || this.token === null)
        {
            console.error("Cannot retrieve backend GET /complain/:id without a token");
            return;
        }

        return new Promise((res, rej) => {
            fetch(`${this._url}/complains/${id}`, {
                headers: {
                    ...this._fetchHeaderToken
                }
            })
                .then(async result => {
                    let complain = await result.json();

                    if (result.status !== 200 || !complain.success)
                        rej(complain.message);

                    res(complain.data);
                })
                .catch(result => rej(result));
        });
    }

    /**
     * Delete the complain
     */
    complain_delete(id) {
        if (this.token === undefined || this.token === null)
        {
            console.error("Cannot retrieve backend PUT /complains/:id without a token");
            return;
        }

        return new Promise((res, rej) => {
            fetch(`${this._url}/complains/${id}`, {
                method: "DELETE",
                headers: {
                    ...this._fetchHeaderToken,
                }
            })
                .then(async result => {
                    let complain = await result.json();

                    if ([200, 201].indexOf(result.status) === -1 || !complain.success)
                        rej(complain.message);

                    res(complain.data);
                })
                .catch(result => rej(result));
        });
    }

    /**
     * Update an existing complain
     */
    complain_update(id, data) {
        if (this.token === undefined || this.token === null)
        {
            console.error("Cannot retrieve backend PUT /complain/:id without a token");
            return;
        }

        return new Promise((res, rej) => {
            fetch(`${this._url}/complains/${id}`, {
                method: "PUT",
                body: JSON.stringify(data),
                headers: {
                    ...this._fetchHeaderToken,
                }
            })
                .then(async result => {
                    let complain = await result.json();

                    if ([200, 201].indexOf(result.status) === -1 || !complain.success)
                        rej(complain.message);

                    res(complain.data);
                })
                .catch(result => rej(result));
        });
    }

    /**
     * Delete the identity
     */
    identity_delete(id) {
        if (this.token === undefined || this.token === null)
        {
            console.error("Cannot retrieve backend PUT /identities/:id without a token");
            return;
        }

        return new Promise((res, rej) => {
            fetch(`${this._url}/identities/${id}`, {
                method: "DELETE",
                headers: {
                    ...this._fetchHeaderToken,
                }
            })
                .then(async result => {
                    let identity = await result.json();

                    if ([200, 201].indexOf(result.status) === -1 || !identity.success)
                        rej(identity.message);

                    res(identity.data);
                })
                .catch(result => rej(result));
        });
    }

    /**
     * Fetching complain identities
     */
    complain_identities(id) {
        if (this.token === undefined || this.token === null)
        {
            console.error("Cannot retrieve backend GET /complains/:id/identities without a token");
            return;
        }

        return new Promise((res, rej) => {
            fetch(`${this._url}/complains/${id}/identities`, {
                headers: {
                    ...this._fetchHeaderToken
                }
            })
                .then(async result => {
                    let complains = await result.json();

                    if (result.status !== 200 || !complains.success)
                        rej(complains.message);

                    res(complains.data);
                })
                .catch(result => rej(result));
        });
    }

    /**
     * Fetching identities
     */
    complains_search(query) {
        if (this.token === undefined || this.token === null)
        {
            console.error("Cannot retrieve backend GET /complains/search without a token");
            return;
        }

        return new Promise((res, rej) => {
            fetch(`${this._url}/complains/search?q=${encodeURI(query)}`, {
                headers: {
                    ...this._fetchHeaderToken
                }
            })
                .then(async result => {
                    let complains
                        = await result.json();

                    if (result.status !== 200 || !complains.success)
                        rej(complains.message);

                    res(complains.data);
                })
                .catch(result => rej(result));
        });
    }

    /**
     * Creating a new complain
     */
    complain_new(data) {
        if (this.token === undefined || this.token === null)
        {
            console.error("Cannot retrieve backend POST /complains/ without a token");
            return;
        }

        return new Promise((res, rej) => {
            fetch(`${this._url}/complains`, {
                method: "POST",
                body: JSON.stringify(data),
                headers: {
                    ...this._fetchHeaderToken,
                }
            })
                .then(async result => {
                    let complain = await result.json();

                    if ([200, 201].indexOf(result.status) === -1 || !complain.success)
                        rej(complain.message);

                    res(complain.data);
                })
                .catch(result => rej(result));
        });
    }
}

export default new backend();