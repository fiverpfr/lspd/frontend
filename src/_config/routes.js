import LoginPage from "../components/LoginPage/LoginPage";
import DashboardHandler from "../components/Dashboard/DashboardHandler";
import ComplainsHandler from "../components/Complains/ComplainsHandler";
import ComplainsSearch from "../components/Complains/ComplainsSearch";
import IdentitiesSearch from "../components/Identities/IdentitiesSearch";
import RedirectToDashboard from "../components/RedirectToDashboard";
import IdentitiesHandler from "../components/Identities/IdentitiesHandler";
import IdentityPage from "../components/Identities/IdentityPage";
import ComplainPage from "../components/Complains/ComplainPage";
import IdentityNew from "../components/Identities/IdentityNew";
import IdentityEdit from "../components/Identities/IdentityEdit";
import ComplainNew from "../components/Complains/ComplainNew";
import ComplainEdit from "../components/Complains/ComplainEdit";
import UsersHandler from "../components/Users/UsersHandler";
import UserNew from "../components/Users/UserNew";
import UserEdit from "../components/Users/UserEdit";

export const routes = [
    {
        name: "Accueil",
        path: "/dashboard",
        exact: false,
        hasSidebar: true,
        isProtected: true,
        component: DashboardHandler,
        subroutes: [
            {
                name: "Rechercher quelqu'un",
                path: "search",
                redirect: "/identities/search"
            },
            {
                name: "Nouvelle identité",
                path: "createidentity",
                redirect: "/identities/create"
            }
        ]
    },
    {
        name: "Accueil",
        path: "/identities",
        exact: false,
        hasSidebar: true,
        isProtected: true,
        component: IdentitiesHandler,
        subroutes: [
            {
                name: "Recherche une identité",
                path: "search",
                component: IdentitiesSearch
            },
            {
                name: "Nouvelle identité",
                path: "create",
                exact: true,
                component: IdentityNew
            },
            {
                name: null,
                path: ":id/edit",
                exact: true,
                component: IdentityEdit
            },
            {
                name: null,
                path: ":id",
                exact: true,
                component: IdentityPage
            }
        ]
    },
    {
        name: "Accueil",
        path: "/complains",
        exact: false,
        hasSidebar: true,
        isProtected: true,
        component: ComplainsHandler,
        subroutes: [
            {
                name: "Rechercher",
                path: "search",
                component: ComplainsSearch
            },
            {
                name: "Nouvelle plainte",
                path: "create",
                exact: true,
                component: ComplainNew
            },
            {
                name: null,
                path: ":id/edit",
                exact: true,
                component: ComplainEdit
            },
            {
                name: null,
                path: ":id",
                exact: true,
                component: ComplainPage
            }
        ]
    },
    {
        name: "Accueil",
        path: "/users",
        exact: false,
        hasSidebar: true,
        isProtected: true,
        component: UsersHandler,
        subroutes: [
            {
                name: "Créer un utilisateur",
                path: "create",
                exact: true,
                component: UserNew
            },
            {
                name: null,
                path: ":id/edit",
                exact: true,
                component: UserEdit
            }
        ]
    },
    {
        path: "/login",
        exact: true,
        hasSidebar: false,
        isProtected: false,
        component: LoginPage
    },
    {
        path: "*",
        exact: true,
        hasSidebar: true,
        isProtected: false,
        component: RedirectToDashboard
    }
];