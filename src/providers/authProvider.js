import React, {Component} from "react";
import backend from "../_config/backend";
import * as PropTypes from "prop-types";
import {toast} from "react-toastify";

export const Context = React.createContext({});

class AuthProvider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userData: undefined,
            mounted: false
        };

        this.actions = {
            onLogin: this.onLogin.bind(this),
            isLoggedIn: this.isLoggedIn.bind(this),
            onLogout: this.onLogout.bind(this),
            isPendingLogin: this.isPendingLogin.bind(this)
        };
    }

    componentDidMount() {
        this.setState({mounted: true});

        if (backend.token === null)
            return;

        this.fetchUserData();
    }

    componentWillUnmount() {
        this.setState({mounted: false});
    }

    fetchUserData() {
        Promise.all([
            backend.me(),
            backend.can("user.create"),
            backend.can("user.update"),
            backend.can("user.toggle"),
            backend.can("user.list"),
            backend.can("complain.delete"),
            backend.can("identity.delete")
        ])
            .then(([me, canCreateUser, canUpdateUser, canToggleUser, canListUser, canDeleteComplain, canDeleteIdentity]) => {
                if (this.state.mounted) {
                    this.updateUserStateData({...me, perms: {
                        "user.create": canCreateUser,
                        "user.update": canUpdateUser,
                        "user.toggle": canToggleUser,
                        "user.list": canListUser,
                        "complain.delete": canDeleteComplain,
                        "identity.delete": canDeleteIdentity
                    }
                    });
                }
            })
            .catch(() => {
                toast.dark("❌ Une erreur s'est produite pendant l'authentification");
                backend.clearToken();
            });
    }

    updateUserStateData(userData) {
        this.setState({userData: userData});
    }

    resetUserStateData() {
        this.setState({userData:undefined});
    }

    isLoggedIn() {
        return backend.token !== null && this.state.userData !== undefined;
    }

    isPendingLogin() {
        return backend.token !== null && this.state.userData === undefined;
    }

    onLogin(username, password) {
        return new Promise((res, rej) => {
            backend.login({username, password})
                .then(authenticated => {

                    if (authenticated)
                    {
                        this.fetchUserData();
                        res();
                        toast.dark("🔫 Vous êtes connecté");
                        return;
                    }

                    this.resetUserStateData();
                    rej();
                })
                .catch(() => {
                    backend.clearToken();
                    toast.dark("❌ Une erreur s'est produite pendant l'authentification");
                    this.resetUserStateData();
                    rej();
                });
        });
    }

    onLogout() {
        if (!this.isLoggedIn())
            return;

        this.resetUserStateData();
        backend.clearToken();
    }

    render() {
        const { children } = this.props;

        return (
            <Context.Provider value={{data: this.state.userData, actions: this.actions}} >
                {children}
            </Context.Provider>
        );
    }
}

AuthProvider.propTypes = {
    children: PropTypes.array.isRequired
};

export default AuthProvider;