import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import Container from "./index.style";

ReactDOM.render(
    <React.StrictMode>
        <Container>
            <App />
        </Container>
    </React.StrictMode>,
    document.getElementById("root")
);
