import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import NavigationBar from "./components/NavigationBar";
import {BrowserRouter, Switch } from "react-router-dom";
import {routes} from "./_config/routes";
import {Container} from "react-bootstrap";
import AuthProvider from "./providers/authProvider";
import CustomRoute from "./components/CustomRoute";
import Loading from "./components/Loading";
import backend from "./_config/backend";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import AppStyle from "./App.style";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            backendHealthy: null,
            backendError: null
        };
    }

    componentDidMount() {
        this.checkHealth();
        setTimeout(() => this.setState({loading: false}), 500);
        this.healthTimer = setInterval(() => this.checkHealth(), 10000);
    }

    componentWillUnmount() {
        clearInterval(this.healthTimer);
    }

    checkHealth() {
        backend.health()
            .then(() => this.state.backendHealthy ? this.setState({backendHealthy: null}) : null)
            .catch(err => this.setState({
                backendHealthy: (typeof err === "object") ? "Le service distant n'est pas prêt" : err
            }
            ));
    }

    render() {
        return (
            <AppStyle>
                <AuthProvider>
                    <ToastContainer
                        position="top-left"
                        autoClose={5000}
                        hideProgressBar={false}
                        newestOnTop
                        closeOnClick
                        draggable
                        pauseOnHover
                        role={"alert"}
                    />
                    <Loading isActive={!!(this.state.loading || this.state.backendHealthy)} text={this.state.backendHealthy || ""} />
                    <BrowserRouter>
                        <Switch>
                            {
                                routes.map((route) => (
                                    <CustomRoute
                                        key={route.path}
                                        path={route.path}
                                        exact={route.exact}
                                        isProtected={route.isProtected}
                                        component={() =>
                                            route.hasSidebar
                                                ? <NavigationBar />
                                                : null
                                        }
                                    />
                                ))
                            }
                        </Switch>
                        <Container fluid="md">
                            <Switch>
                                {
                                    routes.map((route) => (
                                        <CustomRoute
                                            key={route.path}
                                            path={route.path}
                                            exact={route.exact}
                                            isProtected={route.isProtected}
                                            component={route.component}
                                        />
                                    ))
                                }
                            </Switch>
                        </Container>
                    </BrowserRouter>
                </AuthProvider>
            </AppStyle>
        );
    }
}

export default App;
